<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController
{

    /**
     * @Route("/admin")
     */
    public function admin()
    {
        return new Response("ZONA ADMIN");
    }

}
