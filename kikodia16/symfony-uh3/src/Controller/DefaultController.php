<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Translator\EmojiTranslator;

class DefaultController extends AbstractController
{

    /**
     * @Route("/")
     */
    public function index()
    {
        return $this->render('base.html.twig');
    }

    /**
     * @Route("/peliculas")
     */
    public function listadoPeliculas()
    {
        $user = new User('kiko', true);

        return $this->render(
            'peliculas.html.twig',
            [
                'user' => $user
            ]
        );
    }

    /**
     * @Route("/emojear/{texto}", name="convert-text")
     */
    public function demoEmoji(EmojiTranslator $et, $texto){

        // dd($et->translate("Hace sol, al menos no es una caca"));
        return $this->render(
            'emoji.html.twig',
            [
                'frase' => $et->translate($texto)
            ]
        );
    }
}
