<?php

namespace App\Translator;

class EmojiTranslator{

private const EMOJIS = [
    "sol" => "☀",
    "caca" => "💩",
    "prohibido" => "❌"
];
    public function translate($texto){
        return str_replace(array_keys(self::EMOJIS),array_values(self::EMOJIS),$texto);
    }
}