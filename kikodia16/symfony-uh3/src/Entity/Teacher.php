<?php

namespace App\Entity;


class Teacher
{
    protected $name;
    protected $scoring;
    protected $university;

    public function __construct(string $nombre, string $puntuacion, string $univerdad)
    {
        $this->name = $nombre;
        $this->scoring = $puntuacion;
        $this->university = $univerdad;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getScoring()
    {
        return $this->scoring;
    }

    /**
     * @param mixed $scoring
     */
    public function setScoring($scoring): void
    {
        $this->scoring = $scoring;
    }

    /**
     * @return mixed
     */
    public function getUniversity()
    {
        return $this->university;
    }

    /**
     * @param mixed $university
     */
    public function setUniversity($university): void
    {
        $this->university = $university;
    }

    static function cmp_teacher(Teacher $a, Teacher $b)
    {
        if ($a->scoring == $b->scoring){
            return 0;
        }

        return ($a->scoring > $b->scoring)? 1 : -1;
    }
}
