<?php
namespace App\Entity;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class UsuarioFormType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder->add('nombre');
        $builder->add('email');
        $builder->add('nickname');
        $builder->add(
            'tipo',
            ChoiceType::class,
            [
                'choices' => [
                    'User' => 'user',
                    'Admin' => 'admin'
                ],
                'placeholder' => 'Elige tu tipo de usuario'
            ]
        );
    }
}