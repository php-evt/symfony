<?php

namespace App;

class Pelicula{

    public $id;
    public $titulo;
    public $url;
    public $sinopsis;

    public function __construct($id, $titulo, $url, $sinopsis){
        $this->id = $id;
        $this->titulo = $titulo;
        $this->url = $url;
        $this->sinopsis = $sinopsis;
    }

    public function getPelicula(){
        return $this->id;
    }
}