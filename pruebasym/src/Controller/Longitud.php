<?php

namespace App\Controller;
use App\User;
use App\Pelicula;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Longitud extends AbstractController{

    /**
     * @Route("/longitud/{id}", name="Longitud", requirements={"id"="\d+"})
     */
    public function calcularNumero($id){

        if($id % 2 === 0){
            return new Response('el número es par');
        }else{
            return new Response('el número es impar');
        }
    }
    /**
     * @Route("/longitud/{id}", name="Longitud", requirements={"id"="[^/]+"})
     */
        public function palabro($id){
            $words = explode(' ', strtolower($id));
            array_walk($words, function(&$val, $key) {
                $val = str_split($val);
                array_walk($val, function(&$_val, $_key) {
                    $_val = 0 == $_key % 2 ? strtoupper($_val) : $_val;
                });
                $val = implode('', $val);
            });

            return new Response(implode(' ', $words));
        }

        /**
         * @Route("/usuarios")
         */
        public function listadoUsuarios(){
            $usuarios = [
                new User('Edu', 28, true),
                new User('Mery', 29, false),
                new User('Sesal', 31,  true)
            ];

            return $this->render('peliculas.html.twig',['user' => $usuarios ]);
        }

        /**
         * @Route("/peliculas")
         */
        public function listadoPeliculas(){
            $pelis = [
                new Pelicula(1,'Terminator', 'https://m.media-amazon.com/images/M/MV5BYTViNzMxZjEtZGEwNy00MDNiLWIzNGQtZDY2MjQ1OWViZjFmXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Chuache viene del futuro solo para repartir cera.'),
                new Pelicula(2,'Terminator II', 'https://m.media-amazon.com/images/M/MV5BMGU2NzRmZjUtOGUxYS00ZjdjLWEwZWItY2NlM2JhNjkxNTFmXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Chuache vuelve a repartir cera pero ahora es bueno.'),
                new Pelicula(3,'Terminator III', 'https://m.media-amazon.com/images/M/MV5BMTk5NzM1ODgyN15BMl5BanBnXkFtZTcwMzA5MjAzMw@@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Chuache intenta salvar una secuela más que evitable.'),
            ];

            return $this->render('listadopeliculas.html.twig',['peliculas' => $pelis ]);
        }

        /**
         * @Route("/peliculas/{id}")
         */
         public function detallePeliculas($id){

            $pelis = [
                new Pelicula(1,'Terminator', 'https://m.media-amazon.com/images/M/MV5BYTViNzMxZjEtZGEwNy00MDNiLWIzNGQtZDY2MjQ1OWViZjFmXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Chuache viene del futuro solo para repartir cera.'),
                new Pelicula(2,'Terminator II', 'https://m.media-amazon.com/images/M/MV5BMGU2NzRmZjUtOGUxYS00ZjdjLWEwZWItY2NlM2JhNjkxNTFmXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Chuache vuelve a repartir cera pero ahora es bueno.'),
                new Pelicula(3,'Terminator III', 'https://m.media-amazon.com/images/M/MV5BMTk5NzM1ODgyN15BMl5BanBnXkFtZTcwMzA5MjAzMw@@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Chuache intenta salvar una secuela más que evitable.'),
            ];
            return $this->render('detallepeliculas.html.twig',['peli' => $pelis[$id-1] ]);
        }
    }
