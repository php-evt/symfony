<?php

namespace App\Controller;

use App\Entity\User;
use App\Translator\EmojiTranslator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

    /**
     * @Route("/")
     */
    public function index()
    {
        return $this->render('base.html.twig');
    }

    /**
     * @Route("/mi-ruta/{texto}", name="convert-text")
     */
    public function demoEmoji(EmojiTranslator $emojiTranslator, string $texto)
    {
        $textoConvertido = $emojiTranslator->translate($texto);

        return $this->render(
            'emoji/emoji.html.twig',
            [
                'textoOriginal' => $texto,
                'textoConvertido' => $textoConvertido
            ]
        );
    }

    /**
     * @Route("/filtro-emoji/{texto}")
     */
    public function filtroEmoji($texto)
    {
        return $this->render(
            'emoji/filtro-emoji.html.twig',
            [
                'texto' => $texto,
            ]
        );

    }
}
