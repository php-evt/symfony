<?php

namespace App\Controller;

use App\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Teacher;


class UserController extends AbstractController
{

    /**
     * @Route("/users")
     */
    public function listadoUsuarios()
    {
        $usuarios = [
            new User('kiko', true),
            new User('mery', false),
            new User('Celia', false),
        ] ;

        return $this->render(
            'list-users.html.twig',
            [
                'users' => $usuarios,
                'fecha' => new \DateTime("now")
            ]
        );
    }


    /**
     * @Route("sortedteachers")
     */
    public function sortedTeachers(LoggerInterface $logger)
    {
        $logger->info("Entra en el controlador");
        $users = [
            [
                "nombre" => "Moi",
                "puntuacion" => 7.8,
                "universidad" => "Extremadura"
            ],
            [
                "nombre" => "Kiko",
                "puntuacion" => 9,
                "universidad" => "Oviedo"
            ],
            [
                "nombre" => "Jorge",
                "puntuacion" => 2.53,
                "universidad" => "De la vida"
            ],
            [
                "puntuacion" => 3.342,
                "universidad" => "Hogwarts"
            ],
            [
                "nombre" => "Ana",
                "puntuacion" => 7.256,
                "universidad" => "Oviedo"
            ],
        ];

        $teachers = [];

        $teachers[] = new Teacher("Moi", 7.8, "Extremadura");
        $teachers[] = new Teacher("Kiko", 9, "Oviedo");
        $teachers[] = new Teacher("Jorge", 2.53, "De la vida");
        $teachers[] = new Teacher("", 3.342, "Hogwarts");
        $teachers[] = new Teacher("Ana", 7.256, "Oviedo");

        usort($teachers, ["App\Entity\Teacher", "cmp_teacher"]);

        return $this->render(
            'list-teachers.html.twig',
            [
                'teachers' => $teachers,
                'fecha' => new \DateTime("now")
            ]
        );
    }
}
