<?php

namespace App\Controller;

use App\Form\PeliculaFormType;
use App\Form\SerieFormType;
use App\Model\PeliculasManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Serie;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Capitulo;
use App\Entity\Usuario;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AdminController extends AbstractController
{

    /**
     * @Route("/admin/peliculas", name="admin-homepage")
     */
    public function admin(PeliculasManager $peliculasManager)
    {
        //TODO
        return $this->render(
            'admin/list-peliculas.html.twig',
            [
                'peliculas' => $peliculasManager->getPeliculas()
            ]
        );
    }

    /**
     * @Route("/admin/series/new", name="new-serie")
     */
    public function newSerie(Request $request, EntityManagerInterface $em){
        $serieForm = $this->createForm(SerieFormType::class);

        $serieForm->handleRequest($request);

        if ($serieForm->isSubmitted() && $serieForm->isValid()) {
            $serie = $serieForm->getData();

            $this->addFlash('success', '¡Serie creada correctamente!');
            $em->persist($serie);
            $em->flush();
            return $this->redirectToRoute('admin-series');
        }

        return $this->render(
            'admin/new-serie.html.twig',
            [
                'formularioSerie' => $serieForm->createView()
            ]
        );
    }

    /**
     * @Route("/admin/series", name="admin-series")
     */
    public function listSeries(EntityManagerInterface $em){
        $repository = $em->getRepository(Serie::class);
        $serie = $repository->findAll();

        return $this->render(
            'admin/list-series.html.twig',
            [
                'series' => $serie
            ]
        );
    }

    /**
     * @Route("/admin/series/year/{anio}")
     */
    public function listSeriesByYear(EntityManagerInterface $em, $anio){
        $repository = $em->getRepository(Serie::class);
        $serie = $repository->findBy(['year' => $anio]);

        return $this->render(
            'admin/list-peliculas-year.html.twig',
            [
                'series' => $serie
            ]
        );
    }


    /**
     * @Route("/admin/capitulos/{idserie}")
     */
    public function listarCapitulos(EntityManagerInterface $em, $idserie){
        $repository = $em->getRepository(Capitulo::class);
        $cap = $repository->findBy(['serie' => $idserie]);

        // return $this->render(
        //     'admin/list-capitulos.html.twig',
        //     [
        //         'capitulos' => $cap
        //     ]
        // );
        dd($cap);
    }

    /**
     * @Route("/admin/peliculas/new", name="new-pelicula")
     */
    public function newPelicula(Request $request, PeliculasManager $peliculasManager)
    {
        $peliculaForm = $this->createForm(PeliculaFormType::class);

        $peliculaForm->handleRequest($request);

        if ($peliculaForm->isSubmitted() && $peliculaForm->isValid()) {
            $pelicula = $peliculaForm->getData();

            //TODO
            //$peliculasManager->addPelicula($pelicula);

            $this->addFlash('success', '¡Pelicula creada correctamente!');
            return $this->redirectToRoute('admin-homepage');
        }


        return $this->render(
            'admin/new-pelicula.html.twig',
            [
                'formularioPelicula' => $peliculaForm->createView()
            ]
        );
    }

    /**
     * @Route("admin/login", name="app_login")
     */
    public function userLogin(AuthenticationUtils $authenticationUtils)
    {

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $username = $authenticationUtils->getLastUsername();

        return $this->render(
            'admin/login.html.twig',
            [
                'last_username' => $username,
                'error' => $error
            ]
        );
    }


    /**
     * @Route("/admin/peliculas/{id}/edit", name="edit-pelicula")
     */
    public function editPelicula(int $id, Request $request, PeliculasManager $peliculasManager)
    {
        $pelicula = null;

        foreach ($peliculasManager->getPeliculas() as $peli) {
            if ($peli->getId() === $id) {
                $pelicula = $peli;
                break;
            }
        }

        if ($pelicula == null) {
            throw new \InvalidArgumentException("Pelicula inválida");
        }

        $peliculaForm = $this->createForm(PeliculaFormType::class, $pelicula);

        $peliculaForm->handleRequest($request);

        if ($peliculaForm->isSubmitted() && $peliculaForm->isValid()) {
            $pelicula = $peliculaForm->getData();

            //TODO
            //$peliculasManager->addPelicula($pelicula);

            $this->addFlash('success', '¡Pelicula modificada correctamente!');
            return $this->redirectToRoute('admin-homepage');
        }


        return $this->render(
            'admin/edit-pelicula.html.twig',
            [
                'formularioPelicula' => $peliculaForm->createView()
            ]
        );
    }

}
