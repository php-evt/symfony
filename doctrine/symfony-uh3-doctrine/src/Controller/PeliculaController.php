<?php

namespace App\Controller;

use App\Form\PeliculaFormType;
use App\Model\PeliculasManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class PeliculaController extends AbstractController
{
    /**
     * @Route("/peliculas/{id}", name="details-pelicula", methods={"GET"})
     */
    public function detailsPelicula(int $id, PeliculasManager $peliculasManager)
    {
        $peliculas = $peliculasManager->getPeliculas();

        foreach ($peliculas as $pelicula) {
            if ($pelicula->getId() === $id) {
                return $this->render(
                    'pelicula/details.html.twig',
                    [
                        'pelicula' => $pelicula
                    ]
                );
            }
        }
    }

    /**
     * @Route("/peliculas", name="list-peliculas")
     */
    public function listadoPeliculas(PeliculasManager $peliculasManager)
    {
        return $this->render(
            'pelicula/list.html.twig',
            [
                'peliculas' => $peliculasManager->getPeliculas()
            ]
        );
    }

    /**
     * @Route("/peliculas/{id}/like", name="like-pelicula", methods={"PUT"})
     */
    public function likePelicula(int $id, PeliculasManager $peliculasManager)
    {
        $peliculas = $peliculasManager->getPeliculas();

        foreach ($peliculas as $pelicula) {
            if ($pelicula->getId() === $id) {
                $pelicula->like();
                $totalLikes = $pelicula->getLikes();
                break;
            }
        }

        $peliculasManager->savePeliculas($peliculas);

        return new JsonResponse(['likes' => $totalLikes]);
    }
}
