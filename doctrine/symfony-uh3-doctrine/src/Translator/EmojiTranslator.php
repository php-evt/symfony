<?php

namespace App\Translator;


class EmojiTranslator
{
    private const EMOJIS = [
        "sol" => "☀️",
        "caca" => "💩"
    ];


    public function translate(string $texto): string
    {
        return str_replace(array_keys(self::EMOJIS), array_values(self::EMOJIS), $texto);
    }

}
