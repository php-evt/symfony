<?php

namespace App\Model;

use App\Entity\Pelicula;

class PeliculasManager
{

    private $rutaFicheroPeliculas;

    public function __construct(string $rutaFicheroPeliculas)
    {
        $this->rutaFicheroPeliculas = $rutaFicheroPeliculas;
    }

    public function getPeliculas(): array
    {
        $peliculas = [];

        $fd = fopen($this->rutaFicheroPeliculas, 'r');
        while (($linea = fgets($fd)) !== false) {
            $trozos = explode('##', $linea);
            if (count($trozos) == 5) {
                $pelicula = new Pelicula();
                $pelicula->setId(intval($trozos[0]));
                $pelicula->setTitulo($trozos[1]);
                $pelicula->setSinopsis($trozos[2]);
                $pelicula->setUrlImagen($trozos[3]);
                $pelicula->setLikes(intval($trozos[4]));
                $peliculas[] = $pelicula;
            }
        }
        fclose($fd);

        return $peliculas;
    }

    public function savePeliculas(array $peliculas)
    {

        $fd = fopen($this->rutaFicheroPeliculas, 'w');

        foreach ($peliculas as $pelicula) {
            $linea = $pelicula->getId() . '##' . $pelicula->getTitulo() . '##' . $pelicula->getSinopsis() . '##' . $pelicula->getUrlImagen() . '##' . $pelicula->getLikes() . PHP_EOL;
            fputs($fd, $linea);
        }

        fclose($fd);
    }
}
