<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Serie;
use Faker\Factory;

class SerieFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        for($i = 137; $i < 162;  $i++){
            $serie = new Serie();
            $serie->setTitulo($faker->sentence(3));
            $serie->setYear($faker->numberBetween(1980, 2019));
            $manager->persist($serie);
            $this->addReference(Serie::class . $i, $serie);
        }
        $manager->flush();
    }
}
