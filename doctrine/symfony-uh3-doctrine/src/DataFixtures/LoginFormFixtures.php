<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Usuario;
use Faker\Factory;

class LoginFormFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        for($i = 0; $i < 5;  $i++){
            $usuario = new Usuario();
            $usuario->setUsername($faker->sentence(2));
            $usuario->setPassword('123');
            $manager->persist($usuario);
            $this->addReference(Usuario::class . $i, $usuario);
        }
        $manager->flush();
    }
}
