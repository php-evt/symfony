<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\Capitulo;
use App\Entity\Serie;
use Faker\Factory;

class CapituloFixtures extends Fixture implements DependentFixtureInterface
{

    public function getDependencies(){
        return [SerieFixtures::class];
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        for($i = 0; $i < 10;  $i++){
            $cap = new Capitulo();
            $cap->setTitulo($faker->sentence(3));
            $cap->setSinopsis($faker->sentence(10));
            $cap->setValoracion($faker->randomFloat($nbMaxDecimals = 1, $min = 0, $max = 5));
            $serieRef = $this->getReference(Serie::class . $faker->numberBetween(138, 163));
            $cap->setSerie($serieRef);
            $manager->persist($cap);
        }
        $manager->flush();
    }
}
