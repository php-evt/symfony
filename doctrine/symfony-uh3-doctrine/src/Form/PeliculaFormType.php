<?php

namespace App\Form;


use App\Entity\Pelicula;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PeliculaFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titulo');
        $builder->add('sinopsis', TextareaType::class);
        $builder->add('urlImagen');
        $builder->add(
            'categoria',
            ChoiceType::class,
            [
                'choices' => [
                    'Comedia' => 'comedia',
                    'Ciencia ficción' => 'sci-fi'
                ],
                'placeholder' => 'Elije una categoría'
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Pelicula::class]);
    }

}
