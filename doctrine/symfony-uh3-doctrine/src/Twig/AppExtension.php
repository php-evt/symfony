<?php

namespace App\Twig;


use App\Translator\EmojiTranslator;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    private $emojiTranslator;

    public function __construct(EmojiTranslator $emojiTranslator)
    {
        $this->emojiTranslator = $emojiTranslator;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('emoji', [$this, 'translateToEmojis'])
        ];
    }

    public function translateToEmojis($texto)
    {
        return $this->emojiTranslator->translate($texto);
        
    }
}
