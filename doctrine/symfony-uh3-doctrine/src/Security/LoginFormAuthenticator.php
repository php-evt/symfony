<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Security;
use App\Entity\Usuario;

class LoginFormAuthenticator extends AbstractGuardAuthenticator
{
    public function supports(Request $request)
    {
        return true;
    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'username' => 'Et temporibus.',
            'password' => '123'
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['username']
            );
            return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $user = new Usuario();
        $user->setUsername('Et temporibus.');
        $user->setPassword('123');

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return ($credentials['password'] == $user->getPassword());
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        // todo
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // return new RedirectResponse($this->router->generate('admin/series'));
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        // todo
    }

    public function supportsRememberMe()
    {
        // todo
    }
}