<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Pelicula
{
    private $id;
    private $titulo;
    private $sinopsis;
    private $urlImagen;
    private $likes;
    private $categoria;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @Assert\NotBlank(message="El campo no puede estar vacío")
     * @return string
     */
    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    /**
     * @Assert\Length(min="200")
     * @return string
     */
    public function getSinopsis(): ?string
    {
        return $this->sinopsis;
    }

    /**
     * @return string
     */
    public function getUrlImagen(): ?string
    {
        return $this->urlImagen;
    }

    /**
     * @return int
     */
    public function getLikes(): int
    {
        return $this->likes;
    }

    public function like()
    {
        $this->likes++;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo(string $titulo): void
    {
        $this->titulo = $titulo;
    }

    /**
     * @param string $sinopsis
     */
    public function setSinopsis(string $sinopsis): void
    {
        $this->sinopsis = $sinopsis;
    }

    /**
     * @param string $urlImagen
     */
    public function setUrlImagen(string $urlImagen): void
    {
        $this->urlImagen = $urlImagen;
    }

    /**
     * @param int $likes
     */
    public function setLikes(int $likes): void
    {
        $this->likes = $likes;
    }

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria): void
    {
        $this->categoria = $categoria;
    }
}
